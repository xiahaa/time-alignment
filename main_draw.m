% specify where the data is strored
close all;
base = './data';
    
id = 1;
filename = ['GPS_',num2str(id)];
vofilename = ['VO_',num2str(id)];
    
load(fullfile(base,['imugps_',filename,'.mat']));
load(fullfile(base,[vofilename,'.mat']));

data_we_need(:,2) = data_we_need(:,2) - data_we_need(1,2);
data_we_need(:,3) = data_we_need(:,3) - data_we_need(1,3);
data_we_need(:,4) = data_we_need(:,4) - data_we_need(1,4);

horiz_displacement_gps = vecnorm(data_we_need(:,2:3),2,2);
horiz_displacement_vo = vecnorm(vo_we_need(:,[2,4]),2,2);

fid = fopen(['./result/', 'Alignment_', filename, '_', vofilename,'.txt'],'r');
fgetl(fid);
time_offset = str2double(fgetl(fid));
fgetl(fid);
fgetl(fid);
data = split(fgetl(fid));
R(1,:) = [str2double(data{2}), str2double(data{3}), str2double(data{4})];
data = split(fgetl(fid));
R(2,:) = [str2double(data{2}), str2double(data{3}), str2double(data{4})];
data = split(fgetl(fid));
R(3,:) = [str2double(data{2}), str2double(data{3}), str2double(data{4})];
fgetl(fid);
data = split(fgetl(fid));
t = [str2double(data{2}), str2double(data{3}), str2double(data{4})]';
fgetl(fid);
s = str2double(fgetl(fid));
fclose(fid);

figure
plot(data_we_need(:,1), horiz_displacement_gps, 'b-'); hold on;
plot(vo_we_need(:,1)+time_offset, horiz_displacement_vo, 'r--'); hold on;
title('fine alignment');
legend('GNSS','VO');
print(['Time_alignment_',filename,'_',vofilename], '-dpng');

% now we have aligned data, we find R, t to best align them
[id1, id2] = align_time(vo_we_need(:,1),data_we_need(:,1),time_offset);
    

ptsrc = data_we_need(id2, [2,3,4])';
ptdst = vo_we_need(id1, [2,3,4])';
N = size(ptsrc,2);
qt = (R*ptsrc+repmat(t,1,N));
figure
subplot(3,1,1);
plot(data_we_need(id2,1), qt(1,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(1,:), 'r--'); hold on;
title('3D Spatial alignment');
legend('GNSS','VO');
subplot(3,1,2);
plot(data_we_need(id2,1), qt(2,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(2,:), 'r--'); hold on;
subplot(3,1,3);
plot(data_we_need(id2,1), qt(3,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(3,:), 'r--'); hold on;
print(['3D_Spatial_alignment_',filename,'_',vofilename], '-dpng');

eulersimu = data_we_need(id2,[7:9]);
Rimu = angle2dcm(eulersimu(:,3),eulersimu(:,2),eulersimu(:,1));% bodyt to ground
dR = Rimu;
for i = 1:size(Rimu,3)
    dR(:,:,i) = Rimu(:,:,1)' * Rimu(:,:,i);% bodyt to body0
end
% for i = 1:size(Rimu,3)
%     dR(:,:,i) = R * Rimu(:,:,i);% bodyt to vo0
% end

% fid = fopen(['./result/', 'Alignment_1to1_', filename, '_', vofilename,'.txt'],'w');
% for i = 1:size(dR,3)
%     dr = dR(:,:,i);
%     for j = 1:3
%         fprintf(fid,'%12.8f, ',dr(j,:));
%     end
%     fprintf(fid,'%12.8f, %12.8f, %12.8f\n',qt(1,i),qt(2,i),qt(3,i));
% end
% fclose(fid);

tbl = readtable(['./result/', 'Alignment_1to1_', filename, '_', vofilename,'.txt'],'Filetype','text','ReadVariableNames',false);
tbl.Properties.VariableNames = {'R11','R12','R13','R21','R22',...
                                        'R23','R31','R32','R33','t1',...
                                        't2','t3'};
gps_p = [tbl.t1, tbl.t2, tbl.t3]';
figure
subplot(3,1,1);
plot(gps_p(1,:), 'b-'); hold on;
plot(ptdst(1,:), 'r--'); hold on;
title('3D Spatial alignment');
legend('GNSS','VO');
subplot(3,1,2);
plot(gps_p(2,:), 'b-'); hold on;
plot(ptdst(2,:), 'r--'); hold on;
subplot(3,1,3);
plot(gps_p(3,:), 'b-'); hold on;
plot(ptdst(3,:), 'r--'); hold on;


function [id1, id2] = align_time(t1,t2,dt)
    tt1 = t1 + dt;
    meandt = mean(diff(t1));
    id2 = zeros(1,length(t1));
    for i = 1:length(t1)
        [minval, minid] = min(abs(t2-tt1(i)));
        if minval < meandt * 1.00001
            id2(i) = minid;
        end
    end
    id1 = 1:length(t1);
    id1(id2 == 0) = [];
    id2(id2 == 0) = [];
end

