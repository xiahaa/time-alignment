clear all;clc; close all;

%% Load GT for KITTI sequence 09
GT=double(load('./pose.txt'));
vSet_GT = viewSet;
len = 500;%length(GT);
if exist('GT_KITTI_09.mat','file')
for i = 1:len%size(GT,1) 
    if mod(i,100)==0
        disp(['Getting KITTI GT for img ',num2str(i),' out of a total ',num2str(size(GT,1))])
    end
    rot_GT=[GT(i,1),GT(i,2),GT(i,3);GT(i,5),GT(i,6),GT(i,7);GT(i,9),GT(i,10),GT(i,11)]';
    trans_GT=[GT(i,4),GT(i,8),GT(i,12)];

%     rot_GT = rot_GT';
    
    %% do svd here
    [U,~,V] = svd(rot_GT);
    D = V*U';
    if det(D) < 0 
        rot_GT = V*[1 0 0;0 1 0;0 0 -1]*U';
    else
        rot_GT=D;
    end

    % C=C*inv(sqrtm(C'*C))
    % vSet_GT = addView(vSet_GT,1,rigid3d(rot_est,trans_est));
%     vSet_GT = addView(vSet_GT,i,rigid3d(rot_GT,trans_GT));  
    vSet_GT = addView(vSet_GT,i,'Orientation',rot_GT,'Location',trans_GT);  
end
save('GT_KITTI_09.mat','vSet_GT');
else
    load('GT_KITTI_09.mat');
    disp('Ground Truth for KITTI loaded from existing matlab file')
end



%% Load VO estimate path from the python script
path = './BA_files_matlab'

%index for keyframes
data=load(fullfile(path,'\KF_idx.mat'));
KF_idx = data.KF_idx;

% intrinsic
dIntrinsicMatrix=load(fullfile(path,'\intrinsics.mat'));
IntrinsicMatrix = dIntrinsicMatrix.IntrinsicMatrix;

focalLength=[IntrinsicMatrix(1,1),IntrinsicMatrix(2,2)];
principalPoint=[IntrinsicMatrix(1,3),IntrinsicMatrix(2,3)];

imageSize=double(dIntrinsicMatrix.img_size);
intrinsics = cameraIntrinsics(focalLength,principalPoint,imageSize);

% number of images
dnumber_of_images=load(fullfile(path,'\number_images.mat'));
number_images = len;%dnumber_of_images.number_images;

if exist('Poses_VO_v6.mat','file')
KF_idx_idx_last=size(KF_idx,2); %The reason for these double idx is that we talk about the index of a list of all the KeyFrame indexes
KF_idx_idx=1 ;
vSet = viewSet;
set_end_point_same_trans_as_start=true
for i = 1:number_images 
    if mod(i,100)==0
        disp(['Computing img ',num2str(i),' out of a total ',num2str(number_images)])
    end
    
    data=load(fullfile(path,['\FrameID_',num2str(i),'.mat']));

    vSet = addView(vSet,i,'Orientation',double(data.rot),'Location',double(data.trans),'Points',data.KP_left);
    
    if i>1 %For i=1 there is only one image and therefore we can't make any connections
        pairsIdx=uint32(transpose([data.match_queryIdx;data.match_trainIdx]));
        pairsIdx=pairsIdx+1; %Converting from python 0-indexinz to matlab 1-indexing

        if KF_idx_idx~=KF_idx_idx_last; % if on last keyframe we cannot check the next condition "if i>KF_idx(KF_idx_idx+1)" 
            if i>KF_idx(KF_idx_idx+1); % if the current frame is larger than the next keyframe index, this keyframe index has to be chosen as the current one
                KF_idx_idx=KF_idx_idx+1;
            end
        end
        vSet = addConnection(vSet,KF_idx(KF_idx_idx),i,'Matches',pairsIdx);
    end
end
save('Poses_VO_v6.mat','vSet','intrinsics');
else
    load('Poses_VO_v6.mat');
    disp('Loaded since file already exist')
end
% 
cameraPoses = poses(vSet);
GT_Poses = poses(vSet_GT);

%% SHOW RESULTS:
original_trans=vertcat(cameraPoses.Location{:});
GT_trans=vertcat(GT_Poses.Location{:});
original_rot=vertcat(cameraPoses.Orientation{:});
GT_rot=vertcat(GT_Poses.Orientation{:});

Toriginal = cell(size(original_trans,1),1);
Tgt = cell(size(original_trans,1),1);
for i = 1:size(original_trans,1)
    Toriginal{i} = [cameraPoses.Orientation{i} -cameraPoses.Orientation{i}*cameraPoses.Location{i}';0 0 0 1];
    Tgt{i} = [GT_Poses.Orientation{i}' -GT_Poses.Orientation{i}'*GT_Poses.Location{i}';0 0 0 1];
end

% pre-compute some varibales
dxi = zeros(6,length(Toriginal)-1);
for i = 2:length(Toriginal)
    dxi(:,i-1) = tran2vec(inv(Toriginal{i})*Toriginal{i-1});
end

dxiggt = zeros(6,length(Tgt)-1);
for i = 2:length(Tgt)
    dxiggt(:,i-1) = tran2vec(inv(Tgt{i})*Tgt{i-1});
end

figure
for i = 1:6
    subplot(3,2,i);
    plot(dxi(i,:),'r-');hold on;
    plot(dxiggt(i,:),'b-.');hold on;
end



function [ p ] = tran2vec( T )

C = T(1:3,1:3);
r = T(1:3,4);

phi = rot2vec(C);

invJ = vec2jacInv(phi);
%invJ = vec2jacInvSeries(phi,10);

rho = invJ * r;
p = [rho;phi];

end

function [ invJ ] = vec2jacInv( vec )

tolerance = 1e-12;

if size(vec,1) == 3
    
    phi = vec;
    
    ph = norm(phi);
    if ph < tolerance
        % If the angle is small, fall back on the series representation
        invJ = vec2jacInvSeries(phi,10);
    else
        axis = phi/norm(phi);
        ph_2 = 0.5*ph;

        invJ =   ph_2 * cot(ph_2)* eye(3)...
               + (1 - ph_2 * cot(ph_2))* axis * axis'...
               - ph_2 * hat(axis);
    end   
    
elseif size(vec,1) == 6

    rho = vec(1:3);
    phi = vec(4:6);
    
    ph = norm(phi);
    if ph < tolerance;
        % If the angle is small, fall back on the series representation
        invJ = vec2jacInvSeries(phi,10);
        Q = vec2Q( vec );
        invJ = [ invJ -invJ*Q*invJ; zeros(3) invJ ];
    else
        invJsmall = vec2jacInv( phi );
        Q = vec2Q( vec );
        invJ = [ invJsmall -invJsmall*Q*invJsmall; zeros(3) invJsmall ];
    end
    
else   
    warning('vec2jacInv.m:  invalid input vector length\n');   
end   
   
end

function [ b ] = bernoullinumber( k )
    if k == 0 
        b = 1;
    elseif k == 1
        b = -1/2;
    elseif mod(k,2)~= 0 
        b = 0;
    else
        c = 1./factorial(2:k+1);
        b = (-1)^k .*factorial(k).* ...
        det(toeplitz(c, [c(1) 1 zeros(1, k-2)]));
    end
end



function [ invJ ] = vec2jacInvSeries( vec, N )

if size(vec,1) == 3 
    
    invJ = eye(3);
    pxn = eye(3);
    px = hat(vec);
    for n = 1:N
        pxn = pxn * px/n;
        invJ = invJ + bernoullinumber(n) * pxn;
    end
    
elseif size(vec,1) == 6    
    
    invJ = eye(6);
    pxn = eye(6);
    px = curlyhat(vec);
    for n = 1:N
        pxn = pxn * px/n;
        invJ = invJ + bernoullinumber(n) * pxn;
    end
    
else   
    warning('vec2jacInvSeries.m:  invalid input vector length\n');   
end

end

function [ veccurlyhat ] = curlyhat( vec )
    phihat = hat( vec(4:6) );
    veccurlyhat = [ phihat hat(vec(1:3)); zeros(3) phihat ];
end


function [ phi ] = rot2vec( C )

phi = [1;0;0];
[v,d]=eig(C);

cond = (trace(C)-1)/2;
if cond > 1
    cond = 1;
elseif cond < -1
    cond = -1;
end

if abs(cond+1) > 1e-10
    for i=1:3
       if abs(d(i,i)-1) < 1e-10
          a = v(:,i);
          a = a/sqrt(a'*a);
          phim = acos(cond);%(trace(C)-1)/2
          phi = phim*a;

          if abs(trace(vec2rot( phi )'*C)-3) > 1e-14
             phi = -phi;
          end
       end
    end
else
    logA = zeros(3,3);
    for i = 1:2
        logA = logA + (-1)^(i-1)/i.*(C-eye(3))^(i);
    end
    %% project to so3
    logA = 0.5.*(logA-logA');
    phi = [-logA(2,3);logA(1,3);-logA(1,2)];
end

end

function [ C ] = vec2rot( phi )

tolerance = 1e-12;

% Check for a small angle.
% 
angle = norm(phi);
if angle < tolerance
    % If the angle is small, fall back on the series representation.
    % In my experience this is very accurate for small phi
    C = eye(3);
    %display('vec2rot.m:  used series method');
else
    axis = phi/angle;

    cp = cos(angle);
    sp = sin(angle);

    C = cp * eye(3) + (1 - cp) * axis * axis' + sp * hat(axis);
    %display('vec2rot.m:  used analytical method');
end

C = C * inv(sqrtm(C'*C));

end

function vechat = hat(vec)
    if size(vec,1) == 3 
        vechat = [  0,     -vec(3),  vec(2);
                vec(3),   0    , -vec(1);
               -vec(2),  vec(1),   0    ];  
    elseif size(vec,1) == 6
        vechat = [ hat( vec(4:6,1) ) vec(1:3,1); zeros(1,4) ];    
    end   
end




