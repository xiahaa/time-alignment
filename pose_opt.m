clc;clear all;close all;
% load data
load('./pose_data2.mat');

% pre-compute some varibales
fixedid = [1,length(Toriginal)];% fix the first and the last pose, more can be specified
if isempty(find(fixedid == length(Toriginal),1))
    fixedid = [fixedid length(Toriginal)];
end
% for i = 1:length(Toriginal)
%     T = Toriginal{i};
%     Toriginal{i} = [T(1:3,1:3)' T(1:3,1:3)'*T(1:3,4);0 0 0 1];
% end

dxi = zeros(6,length(Toriginal)-1);
curlya = zeros(6, 6, length(Toriginal)-1);
for i = 2:length(Toriginal)
    dxi(:,i-1) = tran2vec(inv(Toriginal{i})*Toriginal{i-1});
    curlya(:,:,i-1) = curlyhat(-dxi(:,i-1)); 
end

dxiggt = zeros(6,length(Tgt)-1);
for i = 2:length(Tgt)
    dxiggt(:,i-1) = tran2vec(inv(Tgt{i})*Tgt{i-1});
end

figure
for i = 1:6
    subplot(3,2,i);
    plot(dxi(i,:),'r-');hold on;
    plot(dxiggt(i,:),'b-.');hold on;
end


Tfixed = cell(length(fixedid),1);
for i = 1:length(fixedid)
    k = fixedid(i);
    Tfixed{i} = Tgt{k};
end

% start pose optimization
% first init with vo
T = Toriginal;
% for i = 1:length(fixedid)
%     k = fixedid(i);
%     T{k} = Tgt{k};
% end

M = 6*length(Toriginal);
N = length(Toriginal);
weight1 = 1;
weight2 = 100;
weight3 = 0;

T = solver_levmar_own(T,M,N,curlya,dxi,fixedid,Toriginal,Tfixed,weight1,weight2,weight3);

original_trans=zeros(N,3);
refined_trans =zeros(N,3);
GT_trans      =zeros(N,3);

for i = 1:N
    Ttemp = Toriginal{i};
    original_trans(i,:) = (-Ttemp(1:3,1:3)'*Ttemp(1:3,4))';
    Ttemp = T{i};
    refined_trans(i,:) = (-Ttemp(1:3,1:3)'*Ttemp(1:3,4))';
    Ttemp = Tgt{i};
    GT_trans(i,:) = (-Ttemp(1:3,1:3)'*Ttemp(1:3,4))';
end

dxiopt = zeros(6,length(T)-1);
for i = 2:length(T)
    dxiopt(:,i-1) = tran2vec(inv(T{i})*T{i-1});
end

figure
for i = 1:6
    subplot(3,2,i);
    plot(dxi(i,:),'r-');hold on;
    plot(dxiggt(i,:),'b-.');hold on;
    plot(dxiopt(i,:),'g--');hold on;
end

figure('Name','2D Paths')
plot(original_trans(:,1),original_trans(:,3),'b','LineWidth',7);hold on;
plot(GT_trans(:,1),GT_trans(:,3),'g','LineWidth',3);
plot(refined_trans(:,1),refined_trans(:,3),'m','LineWidth',3);

function [LHS,RHS,cost] = cost_relative_pose(T,M,N,curlya,dxi,weight,fixedid)
    LHS = zeros(M);
    RHS = zeros(M,1);
    
    cost = 0;
    % cost with regarding to increamental pose
    for j = 1:N-1
        A1 = zeros(M - 6, M);
        b1 = zeros(M - 6, 1);
        
        xi1 = tran2vec(inv(T{j+1})*T{j});
        tmp = 0.5 * curlya(:,:,j);
        b1((j)*6-5:(j)*6) = xi1 - dxi(:,j) + tmp * xi1;
        invJl1 = vec2jacInv( xi1 );
        invJr1 = vec2jacInv( -xi1 );
%         if isempty(find(fixedid==j,1))
            A1((j)*6-5:(j)*6, (j)*6-5:(j)*6) = invJr1 + tmp * invJr1;
%         end
%         if isempty(find(fixedid==(j+1),1))
            A1((j)*6-5:(j)*6, (j)*6+1:(j)*6+6) = -invJl1 - tmp * invJl1;
%         end
        LHS = LHS + weight * (A1'*A1);
        RHS = RHS + weight * A1'*b1;
        cost = cost + weight * (b1'*b1);
    end
end

function [LHS,RHS,cost] = cost_global_pose(T,M,fixedid,Tfixed,weight)
    LHS = zeros(M);
    RHS = zeros(M,1);
    
    cost = 0;
    % cost with regarding to increamental pose
    for j = 1:length(fixedid)
        A2 = zeros(6*length(fixedid), M);
        b2 = zeros(6*length(fixedid), 1);
    
        k = fixedid(j);
        xi3 = tran2vec(inv(Tfixed{j})*T{k});
        b2((j)*6-5:(j)*6) = xi3;
        invJl3 = vec2jacInv( -xi3 );
        A2((j)*6-5:(j)*6, (k)*6-5:(k)*6) = invJl3;
        
        LHS = LHS + weight * (A2'*A2);
        RHS = RHS + weight * A2'*b2;
        cost = cost + weight * (b2'*b2);
    end
end

function [LHS,RHS,cost] = cost_regularization(T,M,N,weight,fixedid)
    LHS = zeros(M);
    RHS = zeros(M,1);
    
    cost = 0;
    % cost with regarding to increamental pose
    for j = 1:N-1
        A1 = zeros(M - 6, M);
        b1 = zeros(M - 6, 1);
    
        xi4 = tran2vec(inv(T{j+1})*T{j});
        b1((j)*6-5:(j)*6) = xi4;
        invJl1 = vec2jacInv( xi4 );
        invJr1 = vec2jacInv( -xi4 );
%         if isempty(find(fixedid==j,1))
            A1((j)*6-5:(j)*6, (j)*6-5:(j)*6) = invJr1;
%         end
%         if isempty(find(fixedid==(j+1),1))
            A1((j)*6-5:(j)*6, (j)*6+1:(j)*6+6) = -invJl1;
%         end
        LHS = LHS + weight * (A1'*A1);
        RHS = RHS + weight * A1'*b1;
        cost = cost + weight * (b1'*b1);
    end
end

function [LHS,RHS,cost] = cost_total(T,M,N,curlya,dxi,fixedid,Toriginal,Tfixed,weight1,weight2,weight3)
    [LHS1,RHS1,cost1] = cost_relative_pose(T,M,N,curlya,dxi,weight1,fixedid);  
%     [LHS1,RHS1,cost1] = cost_global_pose(T,M,1:length(Toriginal),Toriginal,weight1);
    [LHS2,RHS2,cost2] = cost_global_pose(T,M,fixedid,Tfixed,weight2);
    [LHS3,RHS3,cost3] = cost_regularization(T,M,N,weight3,fixedid);
    LHS = LHS1 + LHS2 + LHS3;
    RHS = RHS1 + RHS2 + RHS3;
    cost = cost1+cost2+cost3;
end

function T = solver_levmar_own(T,M,N,curlya,dxi,fixedid,Toriginal,Tfixed,weight1,weight2,weight3)
    k = 0;
    v = 2;
    newT = T;
    [JtJ,Jte,cost] = cost_total(T,M,N,curlya,dxi,fixedid,Toriginal,Tfixed,weight1,weight2,weight3);
    A = JtJ; g = Jte;
    errTol = 1e-6;
    found = max(abs(g)) < errTol;
    tau = 1e-3;
    miu = tau * max(diag(A));
    maxIter = 1000;
    iter = 0;
    epsilon2 = 1e-6;
     % routine for lm
%     disp(['init cost is:',num2str((err'*err))]);
    while found == false && iter < maxIter
        iter = iter + 1;
        hlm = (A+miu*speye(size(A,1),size(A,2)))\(-g);
        hnorm = sqrt(hlm'*hlm);
        if hnorm <= epsilon2
            found = true;
        else
            for j = 1:N
                newT{j} = T{j} * vec2tran( hlm((j)*6-5:(j)*6) );
            end
            [newJtJ,newJte,newcost] = cost_total(newT,M,N,curlya,dxi,fixedid,Toriginal,Tfixed,weight1,weight2,weight3);
            err_gain = cost - newcost;
            gain = err_gain / (0.5*(hlm'*(miu*hlm-g)));
            if gain > 0
                disp(['iter:',num2str(iter),' cost is:',num2str(newcost)]);
                T = newT;
                cost = newcost;
                A = newJtJ;
                g = newJte;
                found = max(abs(newJte)) < errTol;
                miu = miu * max(1/3,1-(2*gain-1)^3);v = 2;
            else
                miu = miu * v;v = v*2;
            end
        end
    end
end

function [ p ] = tran2vec( T )

C = T(1:3,1:3);
r = T(1:3,4);

phi = rot2vec(C);

invJ = vec2jacInv(phi);
%invJ = vec2jacInvSeries(phi,10);

rho = invJ * r;
p = [rho;phi];

end

function [ invJ ] = vec2jacInv( vec )

tolerance = 1e-12;

if size(vec,1) == 3
    
    phi = vec;
    
    ph = norm(phi);
    if ph < tolerance
        % If the angle is small, fall back on the series representation
        invJ = vec2jacInvSeries(phi,10);
    else
        axis = phi/norm(phi);
        ph_2 = 0.5*ph;

        invJ =   ph_2 * cot(ph_2)* eye(3)...
               + (1 - ph_2 * cot(ph_2))* axis * axis'...
               - ph_2 * hat(axis);
    end   
    
elseif size(vec,1) == 6

    rho = vec(1:3);
    phi = vec(4:6);
    
    ph = norm(phi);
    if ph < tolerance;
        % If the angle is small, fall back on the series representation
        invJ = vec2jacInvSeries(phi,10);
        Q = vec2Q( vec );
        invJ = [ invJ -invJ*Q*invJ; zeros(3) invJ ];
    else
        invJsmall = vec2jacInv( phi );
        Q = vec2Q( vec );
        invJ = [ invJsmall -invJsmall*Q*invJsmall; zeros(3) invJsmall ];
    end
    
else   
    warning('vec2jacInv.m:  invalid input vector length\n');   
end   
   
end

function [ b ] = bernoullinumber( k )
    if k == 0 
        b = 1;
    elseif k == 1
        b = -1/2;
    elseif mod(k,2)~= 0 
        b = 0;
    else
        c = 1./factorial(2:k+1);
        b = (-1)^k .*factorial(k).* ...
        det(toeplitz(c, [c(1) 1 zeros(1, k-2)]));
    end
end



function [ invJ ] = vec2jacInvSeries( vec, N )

if size(vec,1) == 3 
    
    invJ = eye(3);
    pxn = eye(3);
    px = hat(vec);
    for n = 1:N
        pxn = pxn * px/n;
        invJ = invJ + bernoullinumber(n) * pxn;
    end
    
elseif size(vec,1) == 6    
    
    invJ = eye(6);
    pxn = eye(6);
    px = curlyhat(vec);
    for n = 1:N
        pxn = pxn * px/n;
        invJ = invJ + bernoullinumber(n) * pxn;
    end
    
else   
    warning('vec2jacInvSeries.m:  invalid input vector length\n');   
end

end

function [ veccurlyhat ] = curlyhat( vec )
    phihat = hat( vec(4:6) );
    veccurlyhat = [ phihat hat(vec(1:3)); zeros(3) phihat ];
end


function [ phi ] = rot2vec( C )

phi = [1;0;0];
[v,d]=eig(C);

cond = (trace(C)-1)/2;
if cond > 1
    cond = 1;
elseif cond < -1
    cond = -1;
end

if abs(cond+1) > 1e-10
    for i=1:3
       if abs(d(i,i)-1) < 1e-10
          a = v(:,i);
          a = a/sqrt(a'*a);
          phim = acos(cond);%(trace(C)-1)/2
          phi = phim*a;

          if abs(trace(vec2rot( phi )'*C)-3) > 1e-14
             phi = -phi;
          end
       end
    end
else
    logA = zeros(3,3);
    for i = 1:2
        logA = logA + (-1)^(i-1)/i.*(C-eye(3))^(i);
    end
    %% project to so3
    logA = 0.5.*(logA-logA');
    phi = [-logA(2,3);logA(1,3);-logA(1,2)];
end

end

function [ C ] = vec2rot( phi )

tolerance = 1e-12;

% Check for a small angle.
% 
angle = norm(phi);
if angle < tolerance
    % If the angle is small, fall back on the series representation.
    % In my experience this is very accurate for small phi
    C = eye(3);
    %display('vec2rot.m:  used series method');
else
    axis = phi/angle;

    cp = cos(angle);
    sp = sin(angle);

    C = cp * eye(3) + (1 - cp) * axis * axis' + sp * hat(axis);
    %display('vec2rot.m:  used analytical method');
end

C = C * inv(sqrtm(C'*C));

end

function vechat = hat(vec)
    if size(vec,1) == 3 
        vechat = [  0,     -vec(3),  vec(2);
                vec(3),   0    , -vec(1);
               -vec(2),  vec(1),   0    ];  
    elseif size(vec,1) == 6
        vechat = [ hat( vec(4:6,1) ) vec(1:3,1); zeros(1,4) ];    
    end   
end


