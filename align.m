% specify where the data is strored
close all;
base = './data';
    
id = 9;
filename = ['GPS_',num2str(id)];
vofilename = ['VO_',num2str(id)];
    
load(fullfile(base,['imugps_',filename,'.mat']));
load(fullfile(base,[vofilename,'.mat']));

data_we_need(:,2) = data_we_need(:,2) - data_we_need(1,2);
data_we_need(:,3) = data_we_need(:,3) - data_we_need(1,3);
data_we_need(:,4) = data_we_need(:,4) - data_we_need(1,4);

spd_diff = diff(data_we_need(1:20:end,[2,3,4])) * 10;
spd_diff = [zeros(1,3);spd_diff];
time_diff = data_we_need(1:20:end,1);
figure
subplot(3,1,1);
plot(time_diff, spd_diff(:,1), 'b-'); hold on;
plot(data_we_need(:,1), data_we_need(:,14), 'r--'); hold on;
title('GNSS Speed');
legend('GNSS Position forward difference','GNSS Vel');
subplot(3,1,2);
plot(time_diff, spd_diff(:,2), 'b-'); hold on;
plot(data_we_need(:,1), data_we_need(:,15), 'r--'); hold on;
subplot(3,1,3);
plot(time_diff, spd_diff(:,3), 'b-'); hold on;
plot(data_we_need(:,1), data_we_need(:,16), 'r--'); hold on;

print(['GNSS_Speed_',filename,'_',vofilename], '-dpng');

% trial
figure
subplot(3,1,1);
plot(data_we_need(:,1), data_we_need(:,2), 'b-'); hold on;
plot(vo_we_need(:,1)+52, -vo_we_need(:,2), 'r--'); hold on;
title('Position');
legend('GNSS','VO');
subplot(3,1,2);
plot(data_we_need(:,1), data_we_need(:,3), 'b-'); hold on;
plot(vo_we_need(:,1)+52, -vo_we_need(:,4), 'r--'); hold on;
subplot(3,1,3);
plot(data_we_need(:,1), data_we_need(:,4), 'b-'); hold on;
plot(vo_we_need(:,1), -vo_we_need(:,3), 'r--'); hold on;
print(['3DPosition_',filename,'_',vofilename], '-dpng');

horiz_displacement_gps = vecnorm(data_we_need(:,2:3),2,2);
horiz_displacement_vo = vecnorm(vo_we_need(:,[2,4]),2,2);
figure
plot(data_we_need(:,1), horiz_displacement_gps, 'b-'); hold on;
plot(vo_we_need(:,1), horiz_displacement_vo, 'r--'); hold on;
title('horiz_displacement');

figure
spd_mag = vecnorm(data_we_need(:,14:15),2,2);
spd_vo = vecnorm(vo_we_need(:,[5,7]),2,2);
plot(data_we_need(:,1), spd_mag, 'b-'); hold on;
h1=plot(vo_we_need(:,1)+52, spd_vo, 'r--'); hold on;
h1.Color(4) = 0.1;
legend('GNSS','VO');
title('Speed');
print(['Speed_',filename,'_',vofilename], '-dpng');

diff_horiz_displacement_vo = diff(horiz_displacement_vo);
diff_horiz_displacement_vo = [0;diff_horiz_displacement_vo];
diff_horiz_displacement_gps = diff(horiz_displacement_gps);
diff_horiz_displacement_gps = [0;diff_horiz_displacement_gps];

valid_vo = abs(horiz_displacement_vo) > max( abs(horiz_displacement_vo))*0.0 & abs(diff_horiz_displacement_vo) > max(abs(diff_horiz_displacement_vo))*0.2;
valid_gps = abs(horiz_displacement_gps) > max( abs(horiz_displacement_gps))*0.0 & abs(diff_horiz_displacement_gps) > max(abs(diff_horiz_displacement_gps))*0.2;

% coarse alignment
time_offset = sync_camera_gyro(horiz_displacement_vo(valid_vo,:)', vo_we_need(valid_vo,1), horiz_displacement_gps(valid_gps,:)', ...
    data_we_need(valid_gps,1)', true, 10);

figure
plot(data_we_need(:,1), horiz_displacement_gps, 'b-'); hold on;
plot(vo_we_need(:,1)+time_offset, horiz_displacement_vo, 'r--'); hold on;
title('coarse alignment');


% fine alignment
options = optimset('Display','iter','MaxIter',100, 'TolX', 1e-3);
fun = @(x) f_alignment_cost(x, horiz_displacement_vo(valid_vo,:), horiz_displacement_gps(valid_gps,:), vo_we_need(valid_vo,1), data_we_need(valid_gps,1));
x0 = time_offset;
x = fminbnd(fun,x0-30, x0+30,options);
time_offset = x;
figure
plot(data_we_need(:,1), horiz_displacement_gps, 'b-'); hold on;
plot(vo_we_need(:,1)+time_offset, horiz_displacement_vo, 'r--'); hold on;
title('fine alignment');
legend('GNSS','VO');
print(['Time_alignment_',filename,'_',vofilename], '-dpng');

% now we have aligned data, we find R, t to best align them
[id1, id2] = align_time(vo_we_need(:,1),data_we_need(:,1),time_offset);
ptsrc = data_we_need(id2, [2,3])';
ptdst = vo_we_need(id1, [2,4])';

if id == 5
    valid_ptsrc = data_we_need(id2,1) >= 60 & data_we_need(id2,1) <= 140;
    ptsrc = ptsrc(:,valid_ptsrc);
    ptdst = ptdst(:,valid_ptsrc);
end

%% ransac to find R, t
    ransac.maxiter = 1e6;
    ransac.threshold = 0.2;% within 0.5 pixel, then it is a inlier
    ransac.prob = 0.99;
    ransac.minimum_sample = 2;
    ransac.bestcost = 0;
    ransac.inliers = [];
    iter = 1;
    
    % data preparation
    N = size(ptsrc,2);
    while iter<ransac.maxiter
        samples = randperm(N,ransac.minimum_sample);
        ps = ptsrc(:,samples);
        qs = ptdst(:,samples);
                
        [R,t,s] = lsq_allign(ps,qs);
        
        if isempty(R)
            iter = iter + 1;
            continue;
        end
        
        % transform
        qt = s.*(R*ptsrc+repmat(t,1,N));
        err = qt - ptdst;
        err = (vecnorm(err));
        % inliers
        inlier_id = err < ransac.threshold;
        inlier_cnt = sum(inlier_id);
        % update
        if ransac.bestcost < inlier_cnt
            ransac.inliers = inlier_id;
            ransac.bestcost = inlier_cnt;
            inlier_percentage = inlier_cnt / N;
            ransac.maxiter = log(0.01)/log(1-inlier_percentage^ransac.minimum_sample);
        end
        iter = iter + 1;
    end
    %% refine with inliers
    ps = ptsrc(:,ransac.inliers);
    qs = ptdst(:,ransac.inliers);
    [R,t,s] = lsq_allign(ps,qs);
    
    ptsrc = data_we_need(id2, [2,3])';
    ptdst = vo_we_need(id1, [2,4])';
    N = size(ptsrc,2);
    qt = s.*(R*ptsrc+repmat(t,1,N));

figure
subplot(2,1,1);
plot(data_we_need(id2,1), qt(1,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(1,:), 'r--'); hold on;
subplot(2,1,2);
plot(data_we_need(id2,1), qt(2,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(2,:), 'r--'); hold on;
title('2D Spatial alignment');

R = [R(1,:) 0;0 0 -1;R(2,:) 0];
t = [t(1);0;t(2)];
ptsrc = data_we_need(id2, [2,3,4])';
ptdst = vo_we_need(id1, [2,3,4])';
qt = s.*(R*ptsrc+repmat(t,1,N));
figure
subplot(3,1,1);
plot(data_we_need(id2,1), qt(1,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(1,:), 'r--'); hold on;
subplot(3,1,2);
plot(data_we_need(id2,1), qt(2,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(2,:), 'r--'); hold on;
subplot(3,1,3);
plot(data_we_need(id2,1), qt(3,:), 'b-'); hold on;
plot(vo_we_need(id1,1)+time_offset, ptdst(3,:), 'r--'); hold on;
legend('GNSS','VO');
title('3D Spatial alignment');
print(['3D_Spatial_alignment_',filename,'_',vofilename], '-dpng');

fid = fopen(['Alignment_',filename,'_',vofilename,'.txt'],'w');
fprintf(fid,'Time Alignment dt (vo_time+dt = gps_time):\n');
fprintf(fid,'%12.8f\n',time_offset);
fprintf(fid,'Spatial Alignment R, t, and scale (P_vo = scale * (R * P_gps + t)):\n');
fprintf(fid,'R:\n');
for i = 1:3
    fprintf(fid,'%12.8f',R(i,:));
    fprintf(fid,'\n');

end
fprintf(fid,'t:\n');
fprintf(fid,'%12.8f',t);
fprintf(fid,'scale:\n');
fprintf(fid,'%12.8f\n',s);
fclose(fid);

%% separate functions
function [R,t,s] = lsq_allign(p,q)
    qm = mean(q,2);
    pm = mean(p,2);
    qd = q - repmat(qm,1,size(q,2));
    pd = p - repmat(pm,1,size(p,2));
    
    % two match one case
    if all(abs(qd(:))) < 1e-6 || all(abs(pd(:))) < 1e-6
        R=[];t=[];s=[];
        return;
    end
    
    s = sqrt(sum(diag(qd'*qd))/sum(diag(pd'*pd)));
%     s = 1;
    qs = qd./s;
    qm = qm./s;
    C = zeros(2,2);
    for i = 1:size(p,2)
        C = C + qs(:,i)*pd(:,i)';
    end
    
    [U,~,V] = svd(C);

    R = U*V';
    if det(R)<0
        R = U*diag([1,-1])*V';
    end
    t = qm - R*pm;
end

function [id1, id2] = align_time(t1,t2,dt)
    tt1 = t1 + dt;
    meandt = mean(diff(t1));
    id2 = zeros(1,length(t1));
    for i = 1:length(t1)
        [minval, minid] = min(abs(t2-tt1(i)));
        if minval < meandt * 1.00001
            id2(i) = minid;
        end
    end
    id1 = 1:length(t1);
    id1(id2 == 0) = [];
    id2(id2 == 0) = [];
end

function cost = f_alignment_cost(x, y1, y2, t1, t2)
    dt = x(1);
    tt1 = t1 + dt;
    meandt = mean(diff(t1));
    alignid = zeros(1,length(t1));
    for i = 1:length(t1)
        [minval, minid] = min(abs(t2-tt1(i)));
        if minval < meandt * 1.00001
            alignid(i) = minid;
        end
    end
    id = 1:length(t1);
    id(alignid == 0) = [];
    alignid(alignid == 0) = [];
    scale = max(y2) / max(y1);
    cost = sum(abs(1*y1(id) - y2(alignid)));
end


function time_offset = sync_camera_gyro(flow, flow_timestamps, gyro_data, ...
    gyro_timestamps, varargin)
% """Get time offset that aligns image timestamps with gyro timestamps. 
%   Given an image sequence, and gyroscope data, with their respective timestamps,
%   calculate the offset that aligns the image data with the gyro data.
%   The timestamps must only differ by an offset, not a scale factor.
% 
%   This function finds an approximation of the offset *d* that makes this transformation
%           t_gyro = t_camera + d
% 
%   i.e. your new image timestamps should be
% 
%           image_timestamps_aligned = image_timestamps + d
% 
%   The offset is calculated using zero-mean cross correlation of the gyroscope data magnitude
%   and the optical flow magnitude, calculated from the image sequence.
%   Multi-scale signal correlation is performed using pyramids to make it quick.
% 
%   The offset is accurate up to about +/- 2 frames, so you should run
%   *refine_time_offset* if you need better accuracy.
% 
%   Parameters
%   ---------------
%       flow : flow strength
%       flow_timestamps : (1,N)
%       gyro_data : (3, N) Gyroscope measurements (angular velocity)
%       gyro_timestamps : (1,N) Timestamps of data in gyro_data     
%       do_plot: 
%       levels : int, Number of pyramid levels
%   Returns
%   --------------
%       time_offset  :  float
%             The time offset to add to image_timestamps to align the image data
%             with the gyroscope data
%     """
    if nargin >= 5
        do_plot = varargin{1};
    else
        do_plot = true;
    end
    if nargin >= 6
        levels = varargin{2};
    else
        levels = 6;
    end
    if nargin >= 7
        save_path = varargin{3};
    else
        save_path = [];
    end
    if nargin >= 8
        manual = varargin{4};
    else
        manual = false;
    end
    
    % Gyro from gyro data
    if size(gyro_data, 1) ~= 1
        gyro_org = vecnorm(gyro_data);
    else
        gyro_org = gyro_data;
    end
    % Resample to match highest
    rate = @(ts) (1/mean(diff(ts)));
    freq_gyro = rate(gyro_timestamps);
    freq_image = rate(flow_timestamps);

    if freq_gyro > freq_image
        rel_rate = freq_gyro / freq_image;
        flow_mag = zncc.upsample(flow, rel_rate);% could also use matlab resample
        gyro_mag = gyro_org;
    else
        flow_mag = flow;
        rel_rate = freq_image / freq_gyro;
        gyro_mag = zncc.upsample(gyro_org, rel_rate);
    end
    
    if ~isempty(save_path)
        save(strcat(save_path,'sig.mat'),'flow_mag','gyro_mag');
    end
    
    gyro_normalized = (gyro_mag / max(gyro_mag));
    flow_normalized = (flow_mag / max(flow_mag));
    
    % semi-autonomous
    if manual
        [flow_ids, gyro_ids] = manual_sync_pick(flow_normalized, gyro_timestamps, gyro_normalized);
        flow_mag_s = flow_mag(flow_ids(1),flow_ids(2));
        gyro_mag_s = gyro_mag(gyro_ids(1),gyro_ids(2));
    else
        flow_mag_s = flow_normalized;
        gyro_mag_s = gyro_normalized;
    end

    ishift = zncc.coarse_to_fine_corr(flow_mag_s,gyro_mag_s, 12, levels);

    if freq_gyro > freq_image
        time_offset = -ishift * 1.0/freq_gyro;
    else
        time_offset = -ishift * 1.0/freq_image;
    end
    
    % First pick good points in flow
    if do_plot
        h = struct();
        h.fig = figure('Name','Time Alignment', 'NumberTitle','off', 'Menubar','none', ...
            'Pointer','cross', 'Resize','on', 'Position',[200 200 400 400]);
        if ~mexopencv.isOctave()
            %HACK: not implemented in Octave
            movegui(h.fig, 'west');
        end
        h.ax = axes('Parent',h.fig, 'Units','normalized', 'Position',[0 0 1 1]);
        subplot(h.ax);
        subplot(2,1,1);
        plot(flow_timestamps, flow/max(flow), 'r-','LineWidth',2);hold on;
        plot(gyro_timestamps, gyro_org/max(gyro_org), 'b-','LineWidth',2);grid on;
        legend({'flow','gyro'});
        title('Before Alignment');
        subplot(2,1,2);
        plot(flow_timestamps+time_offset, flow/max(flow), 'r-','LineWidth',2);hold on;
        plot(gyro_timestamps, gyro_org/max(gyro_org), 'b-','LineWidth',2);grid on;
        legend({'flow','gyro'});
        title('After Alignment');
    end
end                   

