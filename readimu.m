function readimu()
    %% specify where the data is strored
    base = './nouse/raw';
    
    filename = 'GPS_1';
    vofilename = 'VO_1';
    %% constants 
    if ~exist(fullfile(base,['imugps_',filename,'.mat']),'file')
        % do nothing
        linenum = 0;
        fid = fopen([base,'/',filename,'.txt'],'r');
        fseek(fid,-6,'eof');
        a = fgetl(fid);
        fclose(fid);
        tbl = readtable([base,'/',filename,'.txt'],'Filetype','text','ReadVariableNames',false,'HeaderLines',25);

        tbl.Properties.VariableNames = {'GPSTime','X','Y','Z','VelBdyX',...
                                        'VelBdyY','VelBdyZ','AccBdyX','AccBdyY','AccBdyZ',...
                                        'Roll','Pitch','Heading','AngRateX','AngRateY','AngRateZ',...
                                        'AccBiasX','AccBiasY','AccBiasZ','GyroDriftX','GyroDriftY','GyroDriftZ',...
                                        'SDEast','SDNorth','SDHeight'};
    else
        tbl = readtable([filename,'.txt'],'Filetype','text','ReadVariableNames',false);
        tbl.Properties.VariableNames = {'GPSTime','X','Y','Z','VelBdyX',...
                                        'VelBdyY','VelBdyZ','AccBdyX','AccBdyY','AccBdyZ',...
                                        'Roll','Pitch','Heading','AngRateX','AngRateY','AngRateZ',...
                                        'AccBiasX','AccBiasY','AccBiasZ','GyroDriftX','GyroDriftY','GyroDriftZ',...
                                        'SDEast','SDNorth','SDHeight'};
        time = [tbl.GPSTime];
        time = time - time(1);
        time = time / 1e3;% msec to sec

        data_we_need = [tbl.X, tbl.Y, tbl.Z, tbl.VelBdyX,tbl.VelBdyY,...
            tbl.VelBdyZ,tbl.Roll* pi / 180.0,tbl.Pitch* pi / 180.0,tbl.Heading* pi / 180.0,...
            tbl.AngRateX,tbl.AngRateY,tbl.AngRateZ];

        vg = get_vg(data_we_need(:,4:6)', data_we_need(:,7)', data_we_need(:,8)', data_we_need(:,9)');

        data_we_need = [time data_we_need vg'];

        save(fullfile(base,['imugps_',filename,'.mat']),'data_we_need');
    end
    
    %% constants 
    if exist(fullfile(base,[vofilename,'.mat']),'file')
        % do nothing
    else
        tbl = readtable([vofilename,'.txt'],'Filetype','text','ReadVariableNames',false);
        tbl.Properties.VariableNames = {'X','Y','Z'};

        vo_we_need = [tbl.X, tbl.Y, tbl.Z];
        voHz = 30;
        time = ((1:size(vo_we_need,1))-1) * 1/voHz;time = time';
        vo_speed = [zeros(1,3);diff(vo_we_need)] * voHz;
        vo_we_need = [time vo_we_need vo_speed];

        save(fullfile(base,[vofilename,'.mat']),'vo_we_need');
    end
end

function vg = get_vg(vb, roll, pitch, yaw)
    R = angle2dcm(yaw,pitch,roll);
    vg = squeeze(sum(bsxfun(@times, R, reshape(vb,[1,3,length(vb)])), 2));
end

                                                            

function data = txtread(txtfile)
    fid = fopen(txtfile,'r');
    data = [];
    ready = 0;
    while ~feof(fid)
        l = fgetl(fid);
        if ready == 0
            if contains(l,'GPSTime')
                %% skip unit in the next line
                fgetl(fid);
                ready = 1;
            end
        else
            strmat = str2mat(split(l));
            if size(strmat,1) > 20
                data(end+1,:) = str2num(strmat)';
            else
                %% end-of-file may contain some misc information.
            end
        end
    end
    fclose(fid);
end